<?php 
interface Figura{
	public function determinarFigura($altura, $base, $diametro);
}

class Circulo implements Figura{
	public function determinarFigura($altura, $base, $diametro){
		$superficie = ($diametro/2) * 3.14;
		echo "Soy un circulo, mi diámetro es de: {$diametro}cm, mi superficie es de: {$superficie}cm2, no tengo altura ni base.";
	}
}

class Triangulo implements Figura{
	public function determinarFigura($altura, $base, $diametro){
		$superficie = ($altura/2) * $base;
		echo "Soy un triangulo, mi altura es de: {$altura}cm, mi base es de: {$base}cm y mi superficie es de: {$superficie}cm2, no tengo diámetro";
	}
}

class Cuadrado implements Figura{
	public function determinarFigura($altura, $base, $diametro){
		$superficie = $altura * $base;
		echo "Soy un cuadrado, mis lados son todos iguales y miden: {$altura}cm, mi superficie es de: {$superficie}cm2, no tengo diámetro";
	}
}
?>