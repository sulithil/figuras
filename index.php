<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Figuras</title>
	<link rel="stylesheet" href="css/bootstrap.css">
</head>
<body>
	<div class="container">
		<form action="llamadas.php" method="post" accept-charset="utf-8">
			<h2>Indique al menos uno de los siguientes datos</h2>

			<div class="form-group">
				<label for="diametro" class="font-weight-bold">Diámetro</label>
				<input type="number" name="diametro" value="0" min="0" max="999">
			</div>

			<div class="form-group">
				<label for="altura" class="font-weight-bold">Altura</label>
				<input type="number" name="altura" value="0" min="0" max="999">
			</div>

			<div class="form-group">
				<label for="base" class="font-weight-bold">Base</label>
				<input type="number" name="base" value="0" min="0" max="999">
			</div>

			<div class="form-group">
				<input type="submit" name="determinar" value="Determinar" class="btn btn-primary">
			</div>
		</form>
	</div>
	<script src="js/bootstrap.js"></script>
</body>
</html>