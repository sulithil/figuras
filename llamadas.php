<?php
include 'figuraFactory.php';
if (isset($_POST['determinar'])) {
	$diametro = $_POST['diametro'];
	$altura = $_POST['altura'];
	$base = $_POST['base'];

	if ($base == 0 AND $altura == 0 AND $diametro > 0) {
		$tipo = 'circulo';
		$figura = FiguraFactory::datosFigura($tipo, $altura, $base, $diametro);
		$figura->determinarFigura($altura, $base, $diametro);
	} elseif ($base <> $altura AND $base > 0 AND $altura > 0 AND $diametro == 0) {
		$tipo = 'triangulo';
		$figura = FiguraFactory::datosFigura($tipo, $altura, $base, $diametro);
		$figura->determinarFigura($altura, $base, $diametro);
	} elseif ($base == $altura AND $base > 0 AND $altura > 0 AND $diametro == 0) {
		$tipo = 'cuadrado';
		$figura = FiguraFactory::datosFigura($tipo, $altura, $base, $diametro);
		$figura->determinarFigura($altura, $base, $diametro);
	}elseif ($base == 0 AND $altura > 0 OR $base > 0 AND $altura == 0) {
		echo "Si ingresa un valor para la altura o la base debe indicar su otro valor correspondiente";
	}elseif ($base > 0 AND $altura > 0 AND $diametro > 0) {
		echo "Si ingresa un valor en diámetro los valores de altura y base deben ser 0 y si ingreso una altura y una base el diámetro debe ser 0";
	}else {
		echo 'Debe ingresar al menos el diámetro';
	}
			
}

?>